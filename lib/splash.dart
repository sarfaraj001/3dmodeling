import 'dart:async';
import 'package:flutter/material.dart';
// import 'package:flutter_3d_controller/flutter_3d_controller.dart';
import 'package:o3d/o3d.dart';
import 'package:vector_math/vector_math_64.dart' show Vector3;

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _animation;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: 1),
    );

    _animation = Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(_controller)
      ..addListener(() {
        setState(() {});
      });

    _controller.repeat();

    // After a certain delay, navigate to your main screen
    Timer(Duration(seconds: 2), () {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (BuildContext context) => YourMainScreen(),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      body: Center(
        child: Stack(
          children: [
            CustomPaint(
              size: Size(200.0, 200.0),
              painter: _RipplePainter(_animation.value),
            ),

          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}

class _RipplePainter extends CustomPainter {
  final double animationValue;

  _RipplePainter(this.animationValue);

  @override
  void paint(Canvas canvas, Size size) {
    final Paint paint = Paint()
      ..color = Colors.white.withOpacity(0.3 - animationValue * 0.3)
      ..style = PaintingStyle.fill;

    final double radius = size.width * 0.5 * animationValue;
    canvas.drawCircle(Offset(size.width * 0.5, size.height * 0.5), radius, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

class YourMainScreen extends StatefulWidget {
  @override
  State<YourMainScreen> createState() => _YourMainScreenState();
}

class _YourMainScreenState extends State<YourMainScreen> {
  O3DController controller = O3DController();
  String modelFilename = "Mustang.glb";


  @override
  Widget build(BuildContext context) {
    // Your main screen widget
    return Scaffold(
      body: Center(
        child: Container(
          // width: 330,
          // height: 500,
          decoration: BoxDecoration(
            // border: Border.all(color: Colors.grey,width: 1),
            borderRadius: BorderRadius.circular(10)
          ),

          child: O3D(
            src: 'assets/models/Mustang.glb',
            controller: controller,
            ar: false,
            autoPlay: true,
            autoRotate: true,
            cameraControls: true,
            cameraTarget: CameraTarget(-.25, 3, 2.5),
            cameraOrbit: CameraOrbit(0, 90, 1),
          ),
        ),
      ),
    );
  }
}
